package mongoClient;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

/**
 * Java MongoDB : Query document
 *
 * @author Jon Sandberg
 *
 */
public class MongoClientOLD {

	public MongoClientOLD(String string, int i) {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

	try {

	  Mongo mongo = new Mongo("localhost", 27017);
		
	  DB db = mongo.getDB("9spokes");

	  // get a single collection
	  DBCollection collection = db.getCollection("origin_data");

//	  insertDummyDocuments(collection);

	  // Find first document
//	  System.out.println("1. Find first matched document");
//	  DBObject dbObject = collection.findOne();
//	  System.out.println(dbObject);

	  // Find all documents
//	  System.out.println("\n1. Find all matched documents");
//	  DBCursor cursor = collection.find();
//	  while (cursor.hasNext()) {
//		System.out.println(cursor.next());
//	  }

	  // Return specific fields
//	  System.out.println("\n1. Get 'object_origin' field only");
//	  BasicDBObject allQuery = new BasicDBObject();
//	  BasicDBObject fields = new BasicDBObject();
//	  fields.put("object_origin", 1);
//	  fields.put("object_type", 1);
//
//	  DBCursor cursor2 = collection.find(allQuery, fields);
//	  while (cursor2.hasNext()) {
//		System.out.println(cursor2.next());
//	  }

	  // Return documents that match criteria - example 1
//	  System.out.println("\n2. Find where object_origin = googleanalytics");
//	  BasicDBObject whereQuery = new BasicDBObject();
//	  whereQuery.put("object_origin", "googleanalytics");
//	  DBCursor cursor3 = collection.find(whereQuery);
//	  while (cursor3.hasNext()) {
//		System.out.println(cursor3.next());
//	  }

	  // Return documents that match criteria - example 2
	  System.out.println("\n3. Find when object_origin = googleanalytics and object_type = 'website-conversions' example");
	  BasicDBObject andQuery = new BasicDBObject();

	  List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
	  obj.add(new BasicDBObject("object_origin", "googleanalytics"));
	  obj.add(new BasicDBObject("object_type", "website-conversions"));
	  andQuery.put("$and", obj);

	  System.out.println(andQuery.toString());

	  DBCursor cursor7 = collection.find(andQuery);
	  while (cursor7.hasNext()) {
		System.out.println(cursor7.next());
		transformGoogleAnalytics(cursor7.next().toString());
	  }


	  
	  // Close Connection
	  mongo.close();
	  System.out.println("Done");

	 } catch (MongoException e) {
		e.printStackTrace();
	 }

	}
	
	
	public static void transformGoogleAnalytics(String jsonDocument) {

		// Initialise objects and arrays

		JSONParser parser = new JSONParser();
		// rawGoogleAnalyticsJson contains the raw JSON object from Google
		// Analytics
		JSONObject originDataJson = new JSONObject();
		// rawApiDataJson contains the apiData section of the raw JSON
		JSONObject googleAnalyticsApiJson = new JSONObject();

		// utc contains the current timestamp in UTC
		ZonedDateTime utc;
		// profileInfo contains the profileInfo portion of the raw JSON
		JSONObject profileInfo = new JSONObject();

		// rowsArray contains the Rows array (that contains the arrays of data)
		JSONArray rowsArray = new JSONArray();
		// Contains the daily metrics as an array
		JSONArray rowArray = new JSONArray();

		// Contains the daily metrics as an object
		JSONArray metricArray = new JSONArray();
		JSONObject metricObjects = new JSONObject();

		// LOAD DATA
		try {
			originDataJson = (JSONObject) parser.parse(jsonDocument);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		googleAnalyticsApiJson = (JSONObject) originDataJson.get("object_raw_origin_data");

		// TRANSFORMATION CODE - START
		// ==============================================================

		if (googleAnalyticsApiJson != null && googleAnalyticsApiJson.containsKey("profileInfo")) {

			utc = ZonedDateTime.now(ZoneOffset.UTC);
			profileInfo = (JSONObject) googleAnalyticsApiJson.get("profileInfo");

			rowsArray = (JSONArray) googleAnalyticsApiJson.get("rows");
			for (int i = 0; i < rowsArray.size(); i++) {
				rowArray = (JSONArray) rowsArray.get(i);
				JSONObject metricObject = new JSONObject();
				metricObject.put("source", "googleanalytics");
				metricObject.put("dateCreatedUtc", utc.toString());
				metricObject.put("timeDimension", "day");
				metricObject.put("companyId", originDataJson.get("companyId"));
				metricObject.put("accountId", profileInfo.get("accountId"));
				metricObject.put("profileId", profileInfo.get("profileId"));
				metricObject.put("webPropertyId", profileInfo.get("webPropertyId"));
				metricObject.put("userType", rowArray.get(0).toString());
				metricObject.put("date", rowArray.get(1).toString());
				metricObject.put("sessions", rowArray.get(2).toString());
				metricObject.put("pageViews", rowArray.get(3).toString());
				metricObject.put("sessionsDuration", rowArray.get(4).toString());
				metricObject.put("avgSessionDuration", rowArray.get(5).toString());
//				System.out.println(metricObject.toString());
				metricArray.add(metricObject);
			}
		 System.out.println(metricArray.toString());
		}

		metricObjects.put("metrics", metricArray);

		String result = metricObjects.toString();

		System.out.println(toPrettyFormat(result.toString()));

		// TRANSFORMATION CODE - END
		// ==============================================================
		
	}

	public static String toPrettyFormat(String jsonString) {
		// https://coderwall.com/p/ab5qha/convert-json-string-to-pretty-print-java-gson
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);

		return prettyJson;
	}
	
	
	
	
	public static void insertDummyDocuments(DBCollection collection) {

		List<DBObject> list = new ArrayList<DBObject>();

		Calendar cal = Calendar.getInstance();

		for (int i = 1; i <= 5; i++) {

			BasicDBObject data = new BasicDBObject();
			data.append("number", i);
			data.append("name", "jon-" + i);
			// data.append("date", cal.getTime());

			// +1 day
			cal.add(Calendar.DATE, 1);

			list.add(data);

		}

		collection.insert(list);

	}
	
	
}

