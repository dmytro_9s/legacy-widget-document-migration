package mongoClient;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

import org.bson.Document;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MongoClientNew {

	public static void main(String[] args) {

//		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClientURI connectionString = new MongoClientURI("mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");

		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("9spokes");
		MongoCollection<Document> collection = database.getCollection("origin_data");

		 // Create a document
//		 Document doc = new Document("name", "MongoDB")
//	                .append("type", "database")
//	                .append("count", 1)
//	                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
//	                .append("info", new Document("x", 203).append("y", 102));

		 // Insert one document
//		 collection.insertOne(doc);

		 // Create many documents
//		 List<Document> documents = new ArrayList<Document>();
//		 for (int i = 0; i < 100; i++) {
//		     documents.add(new Document("i", i));
//		 }

		 // Insert many documents
//		 collection.insertMany(documents);

		 // Count the documents
		 System.out.println(collection.count());

		 // Find the first document
		 Document myDoc = collection.find().first();
		 System.out.println("The first document is: " + myDoc.toJson());
		 
		 // Find all of the documents
//		 MongoCursor<Document> cursor = collection.find().iterator();
//		 try {
//		     while (cursor.hasNext()) {
//		         System.out.println(cursor.next().toJson());
//		     }
//		 } finally {
//		     cursor.close();
//		 }
		 
		 // Search for a document
//		 myDoc = collection.find(eq("i", 71)).first();
//		 System.out.println(myDoc.toJson());
		 
		 // Get all documents that match a filter
//		 Block<Document> printBlock = new Block<Document>() {
//		     public void apply(final Document document) {
//		         System.out.println(document.toJson());
//		     }
//		};
//		collection.find(gt("i", 50)).forEach(printBlock);
//		collection.find(and(gt("i", 50), lte("i", 100))).forEach(printBlock);

		// Update a single document
//		collection.updateOne(eq("i", 10), new Document("$set", new Document("i", 110)));

		// Update multiple documents
//		UpdateResult updateResult = collection.updateMany(lt("i", 100), inc("i", 100));
//		System.out.println(updateResult.getModifiedCount());
//
//		UpdateResult updateResult2 = collection.updateMany(lt("i", 100), new Document("$inc", new Document("i", 100))); 
//		System.out.println(updateResult2.getModifiedCount());
		
		// Delete one document
//		collection.deleteOne(eq("i", 110));

		// Delete documents that match a filter
//		DeleteResult deleteResult = collection.deleteMany(gte("i", 100));
//		System.out.println(deleteResult.getDeletedCount());
		
		// Create index
//		 collection.createIndex(new Document("i", 1));

		
		 // Close the connection
		 mongoClient.close();
	}

}
