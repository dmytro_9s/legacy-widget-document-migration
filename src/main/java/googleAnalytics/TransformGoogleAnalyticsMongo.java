package googleAnalytics;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static com.mongodb.client.model.Filters.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class TransformGoogleAnalyticsMongo {

	public static void main(String[] args) {

		MongoClientURI connectionString = new MongoClientURI(
				"mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");

		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("9spokes");
		MongoCollection<Document> collection = database.getCollection("origin_data");

		// Get all documents that match a filter
		Block<Document> printBlock = new Block<Document>() {
			public void apply(final Document document) {
				try {
					transformGoogleAnalytics(document.toJson());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		};

		try {
			collection.find(eq("object_origin", "googleanalytics")).forEach(printBlock);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		// Close the connection
		System.out.println("Closing Mongo connection.");
		mongoClient.close();
	}

	@SuppressWarnings("unchecked")
	public static void transformGoogleAnalytics(String jsonDocument) throws ParseException {

		MongoClientURI connectionString2 = new MongoClientURI(
				"mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");
		MongoClient mongoClient2 = new MongoClient(connectionString2);
		MongoDatabase database = mongoClient2.getDatabase("metrics");
		MongoCollection<Document> targetCollection = database.getCollection("google_analytics");

		// Initialise objects and arrays
		JSONParser parser = new JSONParser();

		// rawGoogleAnalyticsJson contains the raw JSON object from Google Analytics
		JSONObject originDataJson = new JSONObject();

		// rawApiDataJson contains the apiData section of the raw JSON
		JSONObject googleAnalyticsApiJson = new JSONObject();

		// utc contains the current timestamp in UTC
		ZonedDateTime utc;

		// profileInfo contains the profileInfo portion of the raw JSON
		JSONObject profileInfo = new JSONObject();

		// columnsArray contains the Rows array (that contains the arrays of data)
		JSONArray columnsArray = new JSONArray();

		// Contains the columnHeader value
		JSONArray columnArray = new JSONArray();

		// rowsArray contains the Rows array (that contains the arrays of data)
		JSONArray rowsArray = new JSONArray();

		// Contains the daily metrics as an array
		JSONArray rowArray = new JSONArray();

		// Contains the daily metrics as an object
		// JSONArray metricArray = new JSONArray();
		// JSONObject metricObjects = new JSONObject();

		// LOAD DATA
		originDataJson = (JSONObject) parser.parse(jsonDocument);
		googleAnalyticsApiJson = (JSONObject) originDataJson.get("object_raw_origin_data");

		// ==============================================================
		// TRANSFORMATION CODE - START
		// ==============================================================

		if (googleAnalyticsApiJson != null 
				&& googleAnalyticsApiJson.containsKey("profileInfo")
				&& googleAnalyticsApiJson.containsKey("rows")) {

			utc = ZonedDateTime.now(ZoneOffset.UTC);
			profileInfo = (JSONObject) googleAnalyticsApiJson.get("profileInfo");
			String containsSampleData = googleAnalyticsApiJson.get("containsSampledData").toString();

			if (containsSampleData == "false") {

				// Iterate over the columnHeaders and add the columnHeaders.name value to a columnArray
				columnsArray = (JSONArray) googleAnalyticsApiJson.get("columnHeaders");
				for (int i = 0; i < columnsArray.size(); i++) {
					JSONObject columnObject = (JSONObject) columnsArray.get(i);
					columnArray.add(columnObject.get("name"));
				}
				
				rowsArray = (JSONArray) googleAnalyticsApiJson.get("rows");
				for (int i = 0; i < rowsArray.size(); i++) {
					rowArray = (JSONArray) rowsArray.get(i);
					JSONObject metricObject = new JSONObject();

					String metricType = null;
					String metricDate = null;
					int metricStartPosition = 0;
					if (columnArray.contains("ga:userType")) {
						metricType = rowArray.get(0).toString() + " Traffic";
						metricDate = rowArray.get(1).toString();
						metricStartPosition = 2;
					} else {
						metricType = "Conversion";
						metricDate = rowArray.get(0).toString();
						metricStartPosition = 1;
					}
					
					metricObject.put("metric_origin", "googleanalytics");
//					metricObject.put("metric_type", metricType.toString());
					metricObject.put("metric_date", metricDate.toString());
					metricObject.put("metric_date_created_utc", utc.toString());
					metricObject.put("metric_time_dimension", "day");
					metricObject.put("tenant_uuid", originDataJson.get("tenant_uuid"));
					metricObject.put("party_uuid", originDataJson.get("party_uuid"));
//					metricObject.put("contains_sample_data", containsSampleData);
					metricObject.put("account_id", profileInfo.get("accountId"));
					metricObject.put("profile_id", profileInfo.get("profileId"));
					metricObject.put("web_property_id", profileInfo.get("webPropertyId"));

					// Iterate through each column heading to capture the heading and value for the metric
					for (int j = metricStartPosition; j < columnArray.size(); j++) {
						String metricKey = metricType + "_" + columnArray.get(j).toString();
						metricObject.put("metric_key",metricKey);
						metricObject.put("metric_value", Double.parseDouble(rowArray.get(j).toString()));
						
						Document doc = Document.parse(metricObject.toString());
						
						targetCollection.updateOne(
							and(eq("party_uuid", originDataJson.get("party_uuid")),
								eq("profile_id", profileInfo.get("profileId")), 
								eq("metric_key", metricKey), 
								eq("metric_date", metricDate), 
								eq("metric_type", metricType)),
							new Document("$set", doc), 
							new UpdateOptions().upsert(true));
					}

				}
			}
		}
		mongoClient2.close();

		// ==============================================================
		// TRANSFORMATION CODE - END
		// ==============================================================

	}

	public static String toPrettyFormat(String jsonString) {
		// https://coderwall.com/p/ab5qha/convert-json-string-to-pretty-print-java-gson
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);

		return prettyJson;
	}

}
