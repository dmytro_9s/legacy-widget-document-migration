package googleAnalytics;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static com.mongodb.client.model.Filters.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class TransformGoogleAnalyticsMongoAllInOneDocument {

	public static void main(String[] args) {

		// MongoClientURI connectionString = new
		// MongoClientURI("mongodb://localhost:27017");
		MongoClientURI connectionString = new MongoClientURI(
				"mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");

		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("9spokes");
		MongoCollection<Document> collection = database.getCollection("origin_data");

		// Count the documents
		// System.out.println("Total count of documents in collection:" +
		// collection.count());

		// Get all documents that match a filter
		Block<Document> printBlock = new Block<Document>() {
			public void apply(final Document document) {
				// System.out.println(document.toJson());
				transformGoogleAnalytics(document.toJson());
			}
		};

		try {
//			collection.find(and(eq("object_origin", "googleanalytics"), eq("object_type", "website-traffic"))).forEach(printBlock);
			collection.find(eq("object_origin", "googleanalytics")).forEach(printBlock);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		// Close the connection
		System.out.println("Closing Mongo connection.");
		mongoClient.close();
	}

	@SuppressWarnings("unchecked")
	public static void transformGoogleAnalytics(String jsonDocument) {

		// MongoClientURI connectionString2 = new
		// MongoClientURI("mongodb://localhost:27017");
		MongoClientURI connectionString2 = new MongoClientURI(
				"mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");
		MongoClient mongoClient2 = new MongoClient(connectionString2);
		MongoDatabase database = mongoClient2.getDatabase("metrics");
		MongoCollection<Document> targetCollection = database.getCollection("google_analytics");

		// Initialise objects and arrays
		JSONParser parser = new JSONParser();

		// rawGoogleAnalyticsJson contains the raw JSON object from Google Analytics
		JSONObject originDataJson = new JSONObject();

		// rawApiDataJson contains the apiData section of the raw JSON
		JSONObject googleAnalyticsApiJson = new JSONObject();

		// utc contains the current timestamp in UTC
		ZonedDateTime utc;

		// profileInfo contains the profileInfo portion of the raw JSON
		JSONObject profileInfo = new JSONObject();

		// columnsArray contains the Rows array (that contains the arrays of data)
		JSONArray columnsArray = new JSONArray();

		// Contains the columnHeader value
		JSONArray columnArray = new JSONArray();

		// rowsArray contains the Rows array (that contains the arrays of data)
		JSONArray rowsArray = new JSONArray();

		// Contains the daily metrics as an array
		JSONArray rowArray = new JSONArray();

		// Contains the daily metrics as an object
		// JSONArray metricArray = new JSONArray();
		// JSONObject metricObjects = new JSONObject();

		// LOAD DATA
		try {
			originDataJson = (JSONObject) parser.parse(jsonDocument);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		googleAnalyticsApiJson = (JSONObject) originDataJson.get("object_raw_origin_data");

		// ==============================================================
		// TRANSFORMATION CODE - START
		// ==============================================================

		if (googleAnalyticsApiJson != null 
				&& googleAnalyticsApiJson.containsKey("profileInfo")
				&& googleAnalyticsApiJson.containsKey("rows")) {
			// Need to filter out sample data "containsSampledData": false

			utc = ZonedDateTime.now(ZoneOffset.UTC);
			profileInfo = (JSONObject) googleAnalyticsApiJson.get("profileInfo");
			String containsSampleData = googleAnalyticsApiJson.get("containsSampledData").toString();

			if (containsSampleData == "false") {

				// Iterate over the columnHeaders and add the columnHeaders.name value to a columnArray
				columnsArray = (JSONArray) googleAnalyticsApiJson.get("columnHeaders");
				for (int i = 0; i < columnsArray.size(); i++) {
					JSONObject columnObject = (JSONObject) columnsArray.get(i);
					columnArray.add(columnObject.get("name"));
				}
				
				rowsArray = (JSONArray) googleAnalyticsApiJson.get("rows");
				for (int i = 0; i < rowsArray.size(); i++) {
					rowArray = (JSONArray) rowsArray.get(i);
					JSONObject metricObject = new JSONObject();

					/* if ... metricType = usertype + "traffic"
					 * else metricType = conversion
					 */
					String metricType = null;
					String metricDate = null;
					if (columnArray.contains("ga:userType")) {
						System.out.println(columnArray.indexOf("userType"));
						metricType = rowArray.get(0).toString() + " Traffic";
						metricDate = rowArray.get(1).toString();
					} else {
						metricType = "Conversion";
						metricDate = rowArray.get(0).toString();
					}
					
					metricObject.put("source", "googleanalytics");
					metricObject.put("metricType", metricType.toString());
					metricObject.put("metricDate", metricDate.toString());
					metricObject.put("dateCreatedUtc", utc.toString());
					metricObject.put("timeDimension", "day");
					metricObject.put("tenant_uuid", originDataJson.get("tenant_uuid"));
					metricObject.put("party_uuid", originDataJson.get("party_uuid"));
					metricObject.put("containsSampleData", containsSampleData);
					metricObject.put("accountId", profileInfo.get("accountId"));
					metricObject.put("profileId", profileInfo.get("profileId"));
					metricObject.put("webPropertyId", profileInfo.get("webPropertyId"));

					// Iterate through each column heading to capture the heading and value for the metric
					for (int j = 0; j < columnArray.size(); j++) {
						metricObject.put(columnArray.get(j).toString(), rowArray.get(j).toString());
					}
					
//					metricObject.put("userType", rowArray.get(0).toString());
//					metricObject.put(columnArray.get(0).toString(), rowArray.get(0).toString());
//					metricObject.put("date", rowArray.get(1).toString());
//					metricObject.put(columnArray.get(1).toString(), rowArray.get(1).toString());
//					metricObject.put("sessions", rowArray.get(2).toString());
//					metricObject.put(columnArray.get(2).toString(), rowArray.get(2).toString());
//					metricObject.put("pageViews", rowArray.get(3).toString());
//					metricObject.put(columnArray.get(3).toString(), rowArray.get(3).toString());
//					metricObject.put("sessionsDuration", rowArray.get(4).toString());
//					metricObject.put(columnArray.get(4).toString(), rowArray.get(4).toString());
//					metricObject.put("avgSessionDuration", rowArray.get(5).toString());
//					metricObject.put(columnArray.get(5).toString(), rowArray.get(5).toString());
					
					// System.out.println(metricObject.toString());
					// metricArray.add(metricObject);
					Document doc = Document.parse(metricObject.toString());
					// targetCollection.insertOne(doc);

					targetCollection.updateOne(
						and(eq("party_uuid", originDataJson.get("party_uuid")),
							eq("profileId", profileInfo.get("profileId")), 
							eq("metricDate", metricDate), 
							eq("metricType", metricType)),
						new Document("$set", doc), 
						new UpdateOptions().upsert(true));
					
					// collection.find(and(eq("object_origin",
					// "googleanalytics"), eq("object_type",
					// "website-traffic"))).forEach(printBlock);

				}
			}
			// System.out.println(metricArray.toString());
		}
		mongoClient2.close();

		// metricObjects.put("metrics", metricArray);

		// String result = metricObjects.toString();
		// System.out.println(toPrettyFormat(result.toString()));

		// ==============================================================
		// TRANSFORMATION CODE - END
		// ==============================================================

	}

	public static String toPrettyFormat(String jsonString) {
		// https://coderwall.com/p/ab5qha/convert-json-string-to-pretty-print-java-gson
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);

		return prettyJson;
	}

}
