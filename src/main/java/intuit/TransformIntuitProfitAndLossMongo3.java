package intuit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static com.mongodb.client.model.Filters.*;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class TransformIntuitProfitAndLossMongo3 {

	// Initialise the parser
	static JSONParser parser = new JSONParser();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		MongoClientURI connectionString = new MongoClientURI(
				 "mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");
//				"mongodb://localhost:27017/");

		// Connect to the data source
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("9spokes");
		MongoCollection<Document> collection = database.getCollection("origin_data");

		// Connect to the transformation destination
		MongoClient mongoClient2 = new MongoClient(connectionString);
		MongoDatabase database2 = mongoClient2.getDatabase("metrics");
		MongoCollection<Document> targetCollection = database2.getCollection("intuit");

		// Find all of the documents
		// {"party_uuid":"29b032bc-0575-4488-aa45-b4a3b991fff3","object_type" :
		// "gross-profit","object_created" :
		// ISODate("2017-06-20T04:38:52.279+0000")}
		MongoCursor<Document> cursor = collection.find(and(eq("object_type", "gross-profit"),
				eq("object_origin", "intuit"), eq("party_uuid", "29b032bc-0575-4488-aa45-b4a3b991fff3"))).iterator();

		// Initialise the metricObject
		JSONObject metricObject = new JSONObject();

		// Process each cursor
		try {
			while (cursor.hasNext()) {

				// Load the current cursor into a JSONObject called cursorObject
				JSONObject cursorObject = (JSONObject) parser.parse(cursor.next().toJson());

				// Skip sample data
				if (cursorObject.get("object_origin").toString().equalsIgnoreCase("sample") != true) {

					metricObject = transformRoot(cursorObject, metricObject);

					JSONObject objectRawOriginDataObject = (JSONObject) cursorObject.get("object_raw_origin_data");
					metricObject = transformRootHeader(objectRawOriginDataObject, metricObject);

					JSONArray reportDatesArray = transformRootColumns(objectRawOriginDataObject, metricObject);

					JSONObject rowsObject = (JSONObject) objectRawOriginDataObject.get("rows");
					JSONArray rowArray = (JSONArray) rowsObject.get("row");

					for (int i = 0; i < rowArray.size(); i++) {
						JSONObject rowObject = (JSONObject) rowArray.get(i);
						metricObject.put("metric_type", rowObject.get("group"));

						if (rowObject.get("type").toString().equalsIgnoreCase("SECTION")
								&& rowObject.containsKey("summary")) {
							JSONObject summaryObject = (JSONObject) rowObject.get("summary");
							metricObject = transformSummary(summaryObject, reportDatesArray, metricObject);
						}
						if (rowObject.get("type").toString().equalsIgnoreCase("SECTION")
								&& rowObject.containsKey("rows")) {
							// process rows object
						}
					}

					System.out.println(metricObject);

				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}

		// Close the connection
		mongoClient.close();

	}

	@SuppressWarnings("unchecked")
	public static JSONObject transformRoot(JSONObject cursorObject, JSONObject metricObject) throws ParseException {
		System.out.println("Transforming Root...");

		JSONObject dateObject = (JSONObject) parser.parse(cursorObject.get("object_created").toString());
		LocalDateTime utcDate = Instant.ofEpochMilli((Long) dateObject.get("$date")).atZone(ZoneId.of("UTC"))
				.toLocalDateTime();

		metricObject.put("object_created", utcDate.toString());
		metricObject.put("metric_origin", cursorObject.get("object_origin"));
		metricObject.put("party_uuid", cursorObject.get("party_uuid"));

		return metricObject;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject transformRootHeader(JSONObject objectRawOriginDataObject, JSONObject metricObject) {
		System.out.println("Transforming Root Header...");

		JSONObject headerObject = (JSONObject) objectRawOriginDataObject.get("header");

		metricObject.put("metric_source_report_name", headerObject.get("reportName"));
		// metricObject.put("report_start_period",headerObject.get("startPeriod"));
		// metricObject.put("report_end_period",headerObject.get("endPeriod"));
		// metricObject.put("report_currency",currency);
		// metricObject.put("report_time",headerObject.get("time"));
		metricObject.put("metric_accounting_basis", headerObject.get("reportBasis"));
		metricObject.put("metric_currency", headerObject.get("currency").toString());

		return metricObject;
	}

	@SuppressWarnings("unchecked")
	public static JSONArray transformRootColumns(JSONObject objectRawOriginDataObject, JSONObject metricObject) {
		System.out.println("Transforming Columns...");

		JSONObject columnsObject = (JSONObject) objectRawOriginDataObject.get("columns");
		JSONArray columnArray = (JSONArray) columnsObject.get("column");
		JSONArray reportDatesArray = new JSONArray();
		for (int i = 0; i < columnArray.size(); i++) {
			JSONObject metadataObject = (JSONObject) columnArray.get(i);
			if (metadataObject.get("colTitle").toString().equalsIgnoreCase("")
					|| metadataObject.get("colTitle").toString().equalsIgnoreCase("Total")) {
				// System.out.println("EMPTY");
			} else {
				// Load the dates from the report into the
				// reportDatesArray
				Date date;
				String currency = metricObject.get("metric_currency").toString();

				try {
					if (currency.equalsIgnoreCase("AUD") || currency.equalsIgnoreCase("GBP")) {
						// AUD
						date = new SimpleDateFormat("dd MMM, yyyy").parse(metadataObject.get("colTitle").toString());
					} else {
						// USD
						date = new SimpleDateFormat("MMM dd, yyyy").parse(metadataObject.get("colTitle").toString());
					}
					String formattedDate = new SimpleDateFormat("yyyyMMdd").format(date);
					reportDatesArray.add(formattedDate);
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return reportDatesArray;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject transformSummary(JSONObject summaryObject, JSONArray reportDatesArray,
			JSONObject metricObject) {
		System.out.println("Transforming section summary...");

		JSONArray colDataArray = (JSONArray) summaryObject.get("colData");
		JSONObject colDataObject = (JSONObject) colDataArray.get(0);
		metricObject.put("metric_key", colDataObject.get("value"));
		for (int i = 1; i < colDataArray.size() - 1; i++) {
			JSONObject valueObject = (JSONObject) colDataArray.get(i);
			metricObject.put("metric_value", valueObject.get("value"));
			metricObject.put("metric_date", reportDatesArray.get(i - 1));
		}
		return metricObject;
	}

	public static void transformRows() {
		System.out.println("Transforming rows data...");

		// JSONObject secondRowsObject = (JSONObject) rowObject.get("rows");
		// JSONArray secondRowArray = (JSONArray) secondRowsObject.get("row");
		// JSONObject secondMetricObject = new JSONObject();
		// for (int sra = 1; sra < secondRowArray.size() - 1; sra++) {
		// JSONObject secondRowObject = (JSONObject) secondRowArray.get(sra);
		// System.out.println(secondRowObject);
		// JSONArray secondColDataArray = (JSONArray)
		// secondRowObject.get("colData");
		// JSONObject secondColDataValue = (JSONObject)
		// secondColDataArray.get(0);
		// String metric_key = (String) secondColDataValue.get("value");
		// Double metric_value = 0.0;
		// for (int scda = 1; scda < secondColDataArray.size() - 1; scda++) {
		// secondColDataValue = (JSONObject) secondColDataArray.get(scda);
		// if (secondColDataValue.get("value").toString().equalsIgnoreCase(""))
		// {
		// // skip
		// } else {
		// metric_value =
		// Double.parseDouble(secondColDataValue.get("value").toString());
		// secondMetricObject.put("metric_date", reportDatesArray.get(scda -
		// 1));
		// secondMetricObject.put("metric_key", metric_key);
		// secondMetricObject.put("metric_value", metric_value);
		// System.out.println(secondMetricObject.toJSONString());
		// }
		// }
		// }
	}

}
