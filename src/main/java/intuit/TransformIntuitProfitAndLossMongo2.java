package intuit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static com.mongodb.client.model.Filters.*;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class TransformIntuitProfitAndLossMongo2 {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		MongoClientURI connectionString = new MongoClientURI(
				// "mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1");
				"mongodb://localhost:27017/");

		// Connect to the data source
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("9spokes");
		MongoCollection<Document> collection = database.getCollection("origin_data");

		// Connect to the transformation destination
		MongoClient mongoClient2 = new MongoClient(connectionString);
		MongoDatabase database2 = mongoClient2.getDatabase("metrics");
		MongoCollection<Document> targetCollection = database2.getCollection("intuit");

		// Find all of the documents
		// {"party_uuid":"29b032bc-0575-4488-aa45-b4a3b991fff3","object_type" : "gross-profit","object_created" : ISODate("2017-06-20T04:38:52.279+0000")}
		MongoCursor<Document> cursor = collection
				.find(and(eq("object_type", "gross-profit"), eq("object_origin", "intuit"), eq("party_uuid", "29b032bc-0575-4488-aa45-b4a3b991fff3"))).iterator();

		JSONParser parser = new JSONParser();
		try {
			while (cursor.hasNext()) {

				// Load the current cursor into a JSONObject called cursorObject
				JSONObject cursorObject = (JSONObject) parser.parse(cursor.next().toJson());

				// Skip sample data
				if (cursorObject.get("object_origin").toString().equalsIgnoreCase("sample")) {
					// System.out.println("SAMPLE!!!");
				} else {
					// Convert the 'object_creation_date' to a UTC Date object
					JSONObject dateObject = (JSONObject) parser.parse(cursorObject.get("object_created").toString());
					LocalDateTime utcDate = Instant.ofEpochMilli((Long) dateObject.get("$date"))
							.atZone(ZoneId.of("UTC")).toLocalDateTime();

					// Initialise the metricObject
					JSONObject metricObject = new JSONObject();

					// Extract the raw data portion
					JSONObject objectRawOriginDataObject = (JSONObject) cursorObject.get("object_raw_origin_data");

					/*
					 * =========================================================
					 * HEADER SECTION
					 * =========================================================
					 * The header section contains report metadata. The end
					 * result is the creation of the metricObject foundation
					 * metadata.
					 */
					JSONObject headerObject = (JSONObject) objectRawOriginDataObject.get("header");

					String currency = headerObject.get("currency").toString();

					/*
					 * =========================================================
					 * COLUMNS SECTION
					 * =========================================================
					 * The columns section contains the list of days included in
					 * the report data. The date format is different for USD and
					 * AUD/GBP. The end result is the reportDatesArray will
					 * contain the list of dates.
					 */
					JSONObject columnsObject = (JSONObject) objectRawOriginDataObject.get("columns");

					JSONArray columnArray = (JSONArray) columnsObject.get("column");
					JSONArray reportDatesArray = new JSONArray();
					for (int i = 0; i < columnArray.size(); i++) {
						JSONObject metadataObject = (JSONObject) columnArray.get(i);
						if (metadataObject.get("colTitle").toString().equalsIgnoreCase("")
								|| metadataObject.get("colTitle").toString().equalsIgnoreCase("Total")) {
							// System.out.println("EMPTY");
						} else {
							// Load the dates from the report into the
							// reportDatesArray
							Date date;
							try {
								if (currency.equalsIgnoreCase("AUD") || currency.equalsIgnoreCase("GBP")) {
									date = new SimpleDateFormat("dd MMM, yyyy")
											.parse(metadataObject.get("colTitle").toString());
								} else {
									date = new SimpleDateFormat("MMM dd, yyyy")
											.parse(metadataObject.get("colTitle").toString());
								}
								String formattedDate = new SimpleDateFormat("yyyyMMdd").format(date);
								reportDatesArray.add(formattedDate);
							} catch (java.text.ParseException e) {
								e.printStackTrace();
							}
						}
					}

					/*
					 * =========================================================
					 * ROWS START
					 * =========================================================
					 * The rows section contains all of the metric data to be
					 * parsed.
					 */
					JSONObject rowsObject = (JSONObject) objectRawOriginDataObject.get("rows");
					JSONArray rowArray = (JSONArray) rowsObject.get("row");
					for (int ra = 0; ra < rowArray.size(); ra++) {
						JSONObject rowObject = (JSONObject) rowArray.get(ra);
						
						if (rowObject.get("type").toString().equalsIgnoreCase("SECTION")){
							System.out.println("SECTION");	
							System.out.println("Contains Summary = " + rowObject.containsKey("summary"));
						} else if (rowObject.get("type").toString().equalsIgnoreCase("DATA")){
							System.out.println("DATA");							
							System.out.println("Contains Summary = " + rowObject.containsKey("summary"));
						} else {
							System.out.println("NADA");
							System.out.println("Contains Summary = " + rowObject.containsKey("summary"));
						}
						
						
						
						
						
						metricObject.put("metric_type", rowObject.get("group"));

						// ===========================================
						// TRANSFORM SUMMARY METRICS - START
						// ===========================================
						System.out.println("SUMMARY METRICS - START");
						JSONObject summaryObject = (JSONObject) rowObject.get("summary");
						JSONArray colDataArray = (JSONArray) summaryObject.get("colData");
						JSONObject colDataObject = (JSONObject) colDataArray.get(0);
						metricObject.put("metric_key", colDataObject.get("value"));
						for (int cda = 1; cda < colDataArray.size() - 1; cda++) {
							JSONObject valueObject = (JSONObject) colDataArray.get(cda);
							metricObject.put("object_created", utcDate.toString());
							metricObject.put("metric_origin", cursorObject.get("object_origin"));
							metricObject.put("party_uuid", cursorObject.get("party_uuid"));
							metricObject.put("metric_source_report_name", headerObject.get("reportName"));
							metricObject.put("metric_accounting_basis", headerObject.get("reportBasis"));
							metricObject.put("metric_currency", currency);
							metricObject.put("metric_value", valueObject.get("value"));
							metricObject.put("metric_date", reportDatesArray.get(cda - 1));

							Document doc = Document.parse(metricObject.toString());

							targetCollection.updateOne(
									and(eq("party_uuid", cursorObject.get("party_uuid")),
											eq("metric_key", colDataObject.get("value")),
											eq("metric_origin", cursorObject.get("object_origin")),
											eq("metric_date", reportDatesArray.get(cda - 1))),
									new Document("$set", doc), new UpdateOptions().upsert(true));
							System.out.println(metricObject.toJSONString());
						}
						System.out.println("SUMMARY METRICS - END");
						// ===========================================
						// TRANSFORM SUMMARY METRICS - END
						// ===========================================

						
						
						// ===========================================
						// TRANSFORM ROWS METRICS - START
						// ===========================================
						System.out.println("ROWS METRICS - START");
// THERE ARE TWO TYPES, SECTION and DATA.

						if (rowObject.containsKey("rows")) {
							JSONObject secondRowsObject = (JSONObject) rowObject.get("rows");
							JSONArray secondRowArray = (JSONArray) secondRowsObject.get("row");
							JSONObject secondMetricObject = new JSONObject();
							for (int sra = 1; sra < secondRowArray.size() - 1; sra++) {
								secondMetricObject.put("metric_type", rowObject.get("group"));
								JSONObject secondRowObject = (JSONObject) secondRowArray.get(sra);
//								System.out.println(secondRowObject);
								JSONArray secondColDataArray = (JSONArray) secondRowObject.get("colData");
								JSONObject secondColDataValue = (JSONObject) secondColDataArray.get(0);
								String metric_key = (String) secondColDataValue.get("value");
								Double metric_value = 0.0;
								for (int scda = 1; scda < secondColDataArray.size() - 1; scda++) {
									secondColDataValue = (JSONObject) secondColDataArray.get(scda);
									if (secondColDataValue.get("value").toString().equalsIgnoreCase("")) {
										// skip
									} else {
										metric_value = Double.parseDouble(secondColDataValue.get("value").toString());
										secondMetricObject.put("object_created", utcDate.toString());
										secondMetricObject.put("metric_origin", cursorObject.get("object_origin"));
										secondMetricObject.put("party_uuid", cursorObject.get("party_uuid"));
										secondMetricObject.put("metric_source_report_name", headerObject.get("reportName"));
										secondMetricObject.put("metric_accounting_basis", headerObject.get("reportBasis"));
										secondMetricObject.put("metric_currency", currency);
										secondMetricObject.put("metric_date", reportDatesArray.get(scda - 1));
										secondMetricObject.put("metric_key", metric_key);
										secondMetricObject.put("metric_value", metric_value);
										
										Document doc = Document.parse(secondMetricObject.toString());

										targetCollection.updateOne(
												and(eq("party_uuid", cursorObject.get("party_uuid")),
														eq("metric_key", colDataObject.get("value")),
														eq("metric_origin", cursorObject.get("object_origin")),
														eq("metric_date", reportDatesArray.get(scda - 1))),
												new Document("$set", doc), new UpdateOptions().upsert(true));
										System.out.println(secondMetricObject.toJSONString());
										
									}
								}
							}

						} else {
							// do nothing
						}

						System.out.println("ROWS METRICS - END");
						// ===========================================
						// TRANSFORM ROWS METRICS - END
						// ===========================================

					} // End of row array
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			cursor.close();
		}

		// Close the connection
		mongoClient.close();

	}

}
