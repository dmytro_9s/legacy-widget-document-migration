package widgets;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.NoSuchElementException;

public class WidgetParser {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		// CONFIGURATION PARAMETERS:
//		String sourceDatabaseConnectionUri = "mongodb://localhost:27017";
//		String targetDatabaseConnectionUri = "mongodb://localhost:27017";
		
		String sourceDatabaseConnectionUri = "mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1";
		String targetDatabaseConnectionUri = "mongodb://root:mongopassword@192.168.70.100:27017/?authSource=admin&authMechanism=SCRAM-SHA-1";
		
		String sourceDatabaseName = "9spokes";
		String sourceCollectionName = "widget_data";
		String tenant_uuid = "00000000-0000-0005-5555-555555555555";

//		String sourceDatabaseName = "widget_data";
//		String sourceCollectionName = "9spokes_widget_data";
//		String tenant_uuid = "00000000-0000-0005-5555-555555555555";

//		String sourceDatabaseName = "widget_data";
//		String sourceCollectionName = "barclays_widget_data";
//		String tenant_uuid = "00000000-0000-0002-2222-222222222222";

//		String sourceDatabaseName = "widget_data";
//		String sourceCollectionName = "deloitteuk_widget_data";
//		String tenant_uuid = "00000000-0000-0006-6666-666666666666";

//		String sourceDatabaseName = "widget_data";
//		String sourceCollectionName = "suncorp_widget_data";
//		String tenant_uuid = "00000000-0000-0007-7777-777777777777";

		String targetDatabaseName = "metrics";
		String targetCollectionName = "widget_metrics";

		// Establish source database connection
		MongoClientURI sourceConnectionString = new MongoClientURI(sourceDatabaseConnectionUri);
		MongoClient sourceMongoClient = new MongoClient(sourceConnectionString);
		MongoDatabase sourceDatabase = sourceMongoClient.getDatabase(sourceDatabaseName);
		MongoCollection<Document> sourceCollection = sourceDatabase.getCollection(sourceCollectionName);

		// Establish target database connection
		MongoClientURI targetConnectionString = new MongoClientURI(targetDatabaseConnectionUri);
		MongoClient targetMongoClient = new MongoClient(targetConnectionString);
		MongoDatabase targetDatabase = targetMongoClient.getDatabase(targetDatabaseName);
		MongoCollection<Document> targetCollection = targetDatabase.getCollection(targetCollectionName);

		// DROP target database collection - DEV PURPOSES ONLY!!!!
//		targetCollection.drop();

		/*
		 * Current set of object_types: attendance-late-show,
		 * attendance-no-show, average-spend-per-sale, booked-leave,
		 * business-growth, campaign-performance, cash-commitments,
		 * cash-position-and-coverage, conversion-rate, days-to-pay,
		 * expenses-to-approve, gross-profit, inventory-value,
		 * inventory-value-trend, jobs-today, money-owed,
		 * money-owed-and-money-owing, money-owing, payroll,
		 * product-gross-profit, project-completion, revenue-per-employee,
		 * sales-and-weather, sales-by-day, sales-by-month, sales-by-week,
		 * sales-staff-performance, shift-tracking, social-following,
		 * staff-utilisation, staff-wage-forecast, storage-usage,
		 * timesheets-to-approve, top-5-expense-categories,
		 * top-5-oldest-expenses-to-approve, top-performing-campaigns,
		 * top-selling-categories, top-selling-products, top-subscription-usage,
		 * total-leads-won-leads, website-conversions, website-traffic,
		 * working-today
		 * 
		 * The following still have parsing exceptions: business-growth,
		 * cash-commitments
		 */

		// Find all of the documents		
		MongoCursor<Document> cursor = sourceCollection.find().noCursorTimeout(true).iterator();
		Integer count = (int) sourceCollection.count();

		// Find specific of the documents		
//		String findFilter = "attendance-late-show";
//		MongoCursor<Document> cursor = sourceCollection.find(eq("object_type", findFilter)).iterator();
//		MongoCursor<Document> cursor = sourceCollection.find(and(eq("object_type", findFilter),(eq("party_uuid", "00000000-0000-0000-0000-000000000000")))).iterator();
//		Integer count = (int) sourceCollection.count(eq("object_type", findFilter));

		Integer countTotal = count;

		JSONParser parser = new JSONParser();
		try {
			while (cursor.hasNext()) {

				// Print number of documents remaining
				System.out.println(count-- + "/" + countTotal);

				// Load the current cursor into a JSONObject called cursorObject
				JSONObject cursorObject = new JSONObject();
				try {
					cursorObject = (JSONObject) parser.parse(cursor.next().toJson());
					System.out.println(cursorObject.toJSONString());

					// Initialise objects;
					JSONObject metricObject = new JSONObject();
					JSONArray dataSetsArray = new JSONArray();
					ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
					JSONObject metricCurrent = new JSONObject();
					JSONObject metricPrevious = new JSONObject();
					JSONArray graphDataArray = new JSONArray();
					JSONArray extrasArray = new JSONArray();

					// START - Create metricObject common values
					utc = ZonedDateTime.now(ZoneOffset.UTC);
					metricObject.put("date_created_utc", utc.toString());

					JSONObject _id = (JSONObject) cursorObject.get("_id");
					metricObject.put("widget_oid", _id.get("$oid"));

					// Convert the 'object_creation_date' to a UTC Date object
					JSONObject dateObject = (JSONObject) parser .parse(cursorObject.get("object_creation_date").toString());
					LocalDateTime utcDate = Instant.ofEpochMilli((Long) dateObject.get("$date")).atZone(ZoneId.of("UTC")).toLocalDateTime();
					metricObject.put("widget_object_creation_date", utcDate.toString());

					metricObject.put("widget_object_origin", cursorObject.get("object_origin"));
					metricObject.put("widget_object_type", cursorObject.get("object_type"));
					metricObject.put("widget_party_uuid", cursorObject.get("party_uuid"));
					metricObject.put("widget_tenant_uuid", tenant_uuid);
					// END - Create metricObject common values
					
					// Build the metricCurrent object with metricObject common values
					metricCurrent = new JSONObject();
					metricCurrent = metricObject;

					// Build the metricPrevious object with metricObject common values
					metricPrevious = new JSONObject();
					metricPrevious = metricObject;
					
					switch (cursorObject.get("object_type").toString()) {
					case "attendance-late-show":
						metricObject.put("metric_date_utc", utcDate.toString());
						if (cursorObject.containsKey("extras")) {
							dataSetsArray = (JSONArray) cursorObject.get("extras");
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
							metricObject.put("widget_array_type", "extras");
							metricObject.put("widget_array_position", 0);

							// The total number of staff members who did not show up
							// for a shift in the last 30 days.
							metricObject.put("metric_key", "total_number_staff_late_show_last_30_days");
							metricObject.put("metric_value", dataSetObject.get("value_1"));
							metricObject.put("metric_time_dimension", "day");
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);

							// The total time of all the shifts that staff did not
							// show for in the last 30 days.
							metricObject.put("metric_key", "total_number_milliseconds_staff_late_show_last_30_days");
							metricObject.put("metric_value", dataSetObject.get("value_2"));
							metricObject.put("metric_time_dimension", "day");
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);
						}
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									// The total number of staff members who did not show up for a shift in the last 30 days.
									metricObject.put("metric_key", "employee_number_staff_late_show_last_30_days");
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("widget_data_entity_type", "employee");
									metricObject.put("widget_data_entity_value",
											dataSetRowObject.get("column_1").hashCode());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);

									// The total time of all the shifts that staff did not show for in the last 30 days.
									metricObject.put("metric_key",
											"employee_number_milliseconds_staff_late_show_last_30_days");
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									metricObject.put("widget_data_entity_type", "employee");
									metricObject.put("widget_data_entity_value",
											dataSetRowObject.get("column_1").hashCode());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "attendance-no-show":
						metricObject.put("metric_date_utc", utcDate.toString());
						if (cursorObject.containsKey("extras")) {
							dataSetsArray = (JSONArray) cursorObject.get("extras");
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
							metricObject.put("widget_array_type", "extras");
							metricObject.put("widget_array_position", 0);

							// The total number of staff members who did not show up for a shift in the last 30 days.
							metricObject.put("metric_key", "total_number_staff_no_show_last_30_days");
							metricObject.put("metric_value", dataSetObject.get("value_1"));
							metricObject.put("metric_time_dimension", "day");
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);

							// The total time of all the shifts that staff did not show for in the last 30 days.
							metricObject.put("metric_key", "total_number_milliseconds_staff_no_show_last_30_days");
							metricObject.put("metric_value", dataSetObject.get("value_2"));
							metricObject.put("metric_time_dimension", "day");
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);
						}
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									// The total number of staff members who did not show up for a shift in the last 30 days.
									metricObject.put("metric_key", "employee_number_staff_no_show_last_30_days");
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("widget_data_entity_type", "employee");
									metricObject.put("widget_data_entity_value",
											dataSetRowObject.get("column_1").hashCode());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);

									// The total time of all the shifts that staff did not show for in the last 30 days.
									metricObject.put("metric_key",
											"employee_number_milliseconds_staff_no_show_last_30_days");
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									metricObject.put("widget_data_entity_type", "employee");
									metricObject.put("widget_data_entity_value",
											dataSetRowObject.get("column_1").hashCode());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "average-spend-per-sale":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("day.this.week")) {
								metricCurrent.put("widget_data_set_name", "day.this.week");
								metricCurrent.put("metric_key", "daily_average_spend_per_sale");
								metricCurrent.put("metric_time_dimension", "day");
							} else {
								metricPrevious.put("widget_data_set_name", "day.last.week");
								metricPrevious.put("metric_key", "daily_average_spend_per_sale");
								metricPrevious.put("metric_time_dimension", "day");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NumberFormatException e) {
//								metricCurrent.put("metric_value", 0);
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
							}
						}
						break;
					case "booked-leave":
						metricObject.put("metric_date_utc", utcDate.toString());
						if (cursorObject.get("data_sets") == null) {
							// Ignore null values
						} else {
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							metricObject.put("metric_time_dimension", "day");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);

									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
									metricObject.put("metric_key", "count_days_booked_leave");
									metricObject.put("widget_data_entity_type", "employee");
									metricObject.put("widget_data_entity_value",
											dataSetRowObject.get("column_1").hashCode());
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "business-growth":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("revenue.growth")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "revenue.growth");
								metricCurrent.put("metric_key", "daily_revenue_growth");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "expenses.growth");
								metricPrevious.put("metric_key", "daily_expenses_growth");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricCurrent.put("metric_value", 0);
								// ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "campaign-performance":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						if (dataSetsArray.isEmpty()) {
							// Ignore empty arrays
						} else {

							if (cursorObject.containsKey("extras")) {
								dataSetsArray = (JSONArray) cursorObject.get("extras");
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
								if (dataSetObject.get("value_1") == null || dataSetObject.get("value_2") == null) {
//									System.out.println("Ignore null values");
								} else {
									metricObject.put("widget_array_type", "extras");
									metricObject.put("widget_array_position", 0);
									metricObject.put("metric_key", "total_open_rate_percentage");
									metricObject.put("metric_value", dataSetObject.get("value_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
									metricObject.put("metric_key", "total_click_rate_percentage");
									metricObject.put("metric_value", dataSetObject.get("value_2"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
							if (cursorObject.containsKey("data_sets")) {
								// Iterate through the data_sets data
								dataSetsArray = (JSONArray) cursorObject.get("data_sets");
								for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
									JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
									JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
									for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
										metricObject.put("widget_array_type", "data_sets");
										metricObject.put("widget_array_position", dsra);
										JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

										if (dataSetRowObject.get("column_2") == null
												|| dataSetRowObject.get("column_3") == null) {
//											System.out.println("Ignoring null values");
										} else {
											metricObject.put("metric_key", "campaign_opened_percentage");
											metricObject.put("metric_value", dataSetRowObject.get("column_2"));
											metricObject.put("widget_data_entity_type", "campaign");
											metricObject.put("widget_data_entity_value",
													dataSetRowObject.get("column_1"));
											updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
													targetCollectionName, targetConnectionString, targetMongoClient,
													targetDatabase, targetCollection, metricObject);
											metricObject.put("metric_key", "campaign_clicked_percentage");
											metricObject.put("metric_value", dataSetRowObject.get("column_3"));
											metricObject.put("widget_data_entity_type", "campaign");
											metricObject.put("widget_data_entity_value",
													dataSetRowObject.get("column_1"));
											updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
													targetCollectionName, targetConnectionString, targetMongoClient,
													targetDatabase, targetCollection, metricObject);
										}
									}
								}
							}
						}
						break;
					case "cash-commitments":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key",
											dataSetRowObject.get("column_1").toString().replaceAll("\\.", "_"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}

						break;
					case "cash-position-and-coverage":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("coverage.this.week")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "coverage.this.week");
								metricCurrent.put("metric_key", "cash_coverage");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "coverage.last.week");
								metricPrevious.put("metric_key", "cash_position");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value", Long.parseLong(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							}
						}
						break;
					case "conversion-rate":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", "conversion_rate_" + dataSetRowObject.get("column_1")
											.toString().replaceAll(" ", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "days-to-pay":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
									if (dataSetRowObject.get("column_2").equals("null")) {
										// IGNORE NULL VALUES
										// System.out.println("NULL");
									} else {
										metricObject.put("metric_value", dataSetRowObject.get("column_2"));
										metricObject.put("metric_key", dataSetRowObject.get("column_1").toString()
												.replaceAll("\\.", "_").toLowerCase());
										updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
												targetConnectionString, targetMongoClient, targetDatabase,
												targetCollection, metricObject);
									}
								}
							}
						}
						break;
					case "expenses-to-approve":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", "expenses_" + dataSetRowObject.get("column_1")
											.toString().replaceAll("\\.", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "gross-profit":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("day.this.week")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "day.this.week");
								metricCurrent.put("metric_key", "gross_profit");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "day.last.week");
								metricPrevious.put("metric_key", "gross_profit");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							}
						}
						break;
					case "inventory-value":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", "inventory_value_" + dataSetRowObject.get("column_1")
											.toString().replaceAll("\\.", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "inventory-value-trend":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("inventory.this.year")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "inventory.this.year");
								metricCurrent.put("metric_key", "inventory_this_year");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "retail.last.year");
								metricPrevious.put("metric_key", "retail_last_year");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "jobs-today":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", "jobs_" + dataSetRowObject.get("column_1").toString()
											.replaceAll("\\.", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "money-owed":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("payables")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "payables");
								metricCurrent.put("metric_key", "money_owed_payables");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "taxes");
								metricPrevious.put("metric_key", "money_owed_taxes");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "money-owed-and-money-owing":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("we.owe")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "we.owe");
								metricCurrent.put("metric_key", "money_we_owe");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "owed.to.us");
								metricPrevious.put("metric_key", "money_owed_to_us");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "money-owing":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						if (cursorObject.containsKey("extras")) {
							dataSetsArray = (JSONArray) cursorObject.get("extras");
							if (dataSetsArray.size() > 0) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
								metricObject.put("widget_array_type", "extras");
								metricObject.put("widget_array_position", 0);
								metricObject.put("metric_key", "total_money_owing");
								metricObject.put("metric_value", dataSetObject.get("value_2"));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricObject);
							}
						}
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									try {
										metricObject.put("widget_array_position", dsra);
										JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
										metricObject.put("metric_key", "company_owing_amount");
										metricObject.put("metric_value", dataSetRowObject.get("column_2"));
										metricObject.put("widget_data_entity_type", "company");
										metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1").hashCode());
										updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
												targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
												metricObject);
									} catch (NullPointerException e) {
										// Ignore null values
										// e.printStackTrace();
									}
								}
							}
						}
						break;
					case "payroll":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("salary.paid")) {
								metricCurrent.put("metric_time_dimension", "day");
								metricCurrent.put("widget_data_set_name", "salary.paid");
								metricCurrent.put("metric_key", "payroll_salary_paid");
							} else {
								metricPrevious.put("metric_time_dimension", "day");
								metricPrevious.put("widget_data_set_name", "staff");
								metricPrevious.put("metric_key", "payroll_staff");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value", Double.parseDouble(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "product-gross-profit":
						metricObject.put("metric_date_utc", utcDate.toString());
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						metricObject.put("metric_time_dimension", "day");
						for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
							JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
							if (dsa == 0) {
								metricObject.put("metric_key", "best_product_gross_profit");
							} else {
								metricObject.put("metric_key", "worst_product_gross_profit");
							}
							if (dataSetRowsArray.size() > 0) {
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);

									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
									if (dataSetRowObject.get("column_2") == null) {
//										System.out.println("Ignore null values.");
									} else {
										metricObject.put("widget_data_entity_type", "product");
										metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
										metricObject.put("metric_value", dataSetRowObject.get("column_2"));
										updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
												targetConnectionString, targetMongoClient, targetDatabase,
												targetCollection, metricObject);
									}
								}
							}
						}
						break;
					case "project-completion":
						metricObject.put("metric_date_utc", utcDate.toString());
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						metricObject.put("metric_time_dimension", "day");
						for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
							JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
							if (dsa == 0) {
								metricObject.put("metric_key", "project_near_due");
							} else {
								metricObject.put("metric_key", "project_oldest_overdue");
							}
							if (dataSetRowsArray.size() > 0) {
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);

									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
									metricObject.put("widget_data_entity_type", "project");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "revenue-per-employee":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("revenue.this.week")) {
								metricCurrent.put("widget_data_set_name", "revenue.this.week");
								metricCurrent.put("metric_key", "employee_revenue");
								metricCurrent.put("metric_time_dimension", "day");
							} else {
								metricPrevious.put("widget_data_set_name", "revenue.last.week");
								metricPrevious.put("metric_key", "employee_revenue");
								metricPrevious.put("metric_time_dimension", "day");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value", Double.parseDouble(extrasObject.get("value_2").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					// case "sales-and-weather":
					// System.out.println(cursorObject.get("object_type").toString());
					// break;
					case "sales-by-day":

						if (cursorObject.get("graph_data") == null){
//							System.out.println("NULL");
						} else {
							// Iterate through the graph data
							graphDataArray = (JSONArray) cursorObject.get("graph_data");
							for (int gda = 0; gda < graphDataArray.size(); gda++) {
								JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
								String dataSetName = (String) graphArrayObject.get("data_set_name");

								if (dataSetName.equals("day.this.week")) {
									metricCurrent.put("widget_data_set_name", "day.this.week");
									metricCurrent.put("metric_key", "sales");
									metricCurrent.put("metric_time_dimension", "day");
								} else {
									metricPrevious.put("widget_data_set_name", "day.last.week");
									metricPrevious.put("metric_key", "sales");
									metricPrevious.put("metric_time_dimension", "day");
								}
							}
							// Iterate through the metric data
							extrasArray = (JSONArray) cursorObject.get("extras");
							for (int ea = 0; ea < extrasArray.size(); ea++) {
								JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
								metricCurrent.put("widget_array_type", "extras");
								metricCurrent.put("widget_array_position", ea);
								metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
								try {
									metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
									metricCurrent.put("metric_value",
											Double.parseDouble(extrasObject.get("value_1").toString()));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
											targetCollectionName, targetConnectionString, targetMongoClient,
											targetDatabase, targetCollection, metricCurrent);
								} catch (NullPointerException e) {
									//								metricCurrent.put("metric_value", 0);
									// Ignore null values
								} catch (NumberFormatException e) {
									//								metricPrevious.put("metric_value", 0);
									// Ignore invalid values
								}

								metricPrevious.put("widget_array_type", "extras");
								metricPrevious.put("widget_array_position", ea);
								metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
								try {
									metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
									metricPrevious.put("metric_value",
											Double.parseDouble(extrasObject.get("value_2").toString()));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
											targetCollectionName, targetConnectionString, targetMongoClient,
											targetDatabase, targetCollection, metricPrevious);
								} catch (NullPointerException e) {
									//								metricPrevious.put("metric_value", 0);
									// Ignore null values
								} catch (NumberFormatException e) {
									//								metricPrevious.put("metric_value", 0);
									// Ignore invalid values
								}
							} 
						}
						break;
					case "sales-by-month":
						
						if (cursorObject.get("graph_data") == null) {
							// do nothing
//							System.out.println("null");
						} else
						{
							// Iterate through the graph data
							graphDataArray = (JSONArray) cursorObject.get("graph_data");
							for (int gda = 0; gda < graphDataArray.size(); gda++) {
								JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
								String dataSetName = (String) graphArrayObject.get("data_set_name");

								if (dataSetName.equals("month.this.year")) {
									metricCurrent.put("widget_data_set_name", "month.this.year");
									metricCurrent.put("metric_key", "sales");
									metricCurrent.put("metric_time_dimension", "month");
								} else {
									metricPrevious.put("widget_data_set_name", "month.last.year");
									metricPrevious.put("metric_key", "sales");
									metricPrevious.put("metric_time_dimension", "month");
								}
							}
							// Iterate through the metric data
							extrasArray = (JSONArray) cursorObject.get("extras");
							for (int ea = 0; ea < extrasArray.size(); ea++) {
								JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
								metricCurrent.put("widget_array_type", "extras");
								metricCurrent.put("widget_array_position", ea);
								metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
								try {
									metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
									metricCurrent.put("metric_value",
											Double.parseDouble(extrasObject.get("value_1").toString()));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
											targetCollectionName, targetConnectionString, targetMongoClient,
											targetDatabase, targetCollection, metricCurrent);
								} catch (NullPointerException e) {
									//								metricCurrent.put("metric_value", 0);
									// Ignore null values
								} catch (NumberFormatException e) {
									//								metricPrevious.put("metric_value", 0);
									// Ignore invalid values
								}

								metricPrevious.put("widget_array_type", "extras");
								metricPrevious.put("widget_array_position", ea);
								metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
								try {
									metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
									metricPrevious.put("metric_value",
											Double.parseDouble(extrasObject.get("value_1").toString()));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
											targetCollectionName, targetConnectionString, targetMongoClient,
											targetDatabase, targetCollection, metricPrevious);
								} catch (NullPointerException e) {
									//								metricPrevious.put("metric_value", 0);
									// Ignore null values
								} catch (NumberFormatException e) {
									//								metricPrevious.put("metric_value", 0);
									// Ignore invalid values
								}
							} 
						}
						break;
					case "sales-by-week":
						// System.out.println(cursorObject.get("object_type").toString());

						// Convert the 'object_creation_date' to a UTC Date
						// object
						dateObject = (JSONObject) parser.parse(cursorObject.get("object_creation_date").toString());
						utcDate = Instant.ofEpochMilli((Long) dateObject.get("$date")).atZone(ZoneId.of("UTC"))
								.toLocalDateTime();

						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("week.this.year")) {
								metricCurrent.put("widget_data_set_name", "week.this.year");
								metricCurrent.put("metric_key", "sales");
								metricCurrent.put("metric_time_dimension", "week");
							} else {
								metricPrevious.put("widget_data_set_name", "week.last.year");
								metricPrevious.put("metric_key", "sales");
								metricPrevious.put("metric_time_dimension", "week");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invaliv values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "sales-staff-performance":
						metricObject.put("metric_date_utc", utcDate.toString());
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						metricObject.put("metric_time_dimension", "day");
						for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
							JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
							if (dsa == 0) {
								metricObject.put("metric_key", "top_sales_staff_performance");
							} else {
								metricObject.put("metric_key", "bottom_sales_staff_performance");
							}
							if (dataSetRowsArray.size() > 0) {
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									try {
										metricObject.put("widget_array_type", "data_sets");
										metricObject.put("widget_array_position", dsra);

										JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
										metricObject.put("widget_data_entity_type", "employee");
										metricObject.put("widget_data_entity_value",
												dataSetRowObject.get("column_1").hashCode());
										metricObject.put("metric_value", dataSetRowObject.get("column_2"));
										updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
												targetConnectionString, targetMongoClient, targetDatabase,
												targetCollection, metricObject);
									} catch (NullPointerException e) {
										// System.out.println("Null values being
										// ignored...");
//										e.printStackTrace();
									}
								}
							}
						}
						break;
					case "shift-tracking":
						metricObject.put("metric_date_utc", utcDate.toString());
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						metricObject.put("metric_time_dimension", "day");
						for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
							JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
							if (dsa == 0) {
								metricObject.put("metric_key", "normal_shifts_completed");
							} else {
								metricObject.put("metric_key", "overtime_shifts_completed");
							}
							if (dataSetRowsArray.size() > 0) {
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									try {
										metricObject.put("widget_array_type", "data_sets");
										metricObject.put("widget_array_position", dsra);

										JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
										metricObject.put("widget_data_entity_type", "employee");
										metricObject.put("widget_data_entity_value",
												dataSetRowObject.get("column_1").hashCode());
										metricObject.put("metric_value", dataSetRowObject.get("column_2"));
										updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
												targetConnectionString, targetMongoClient, targetDatabase,
												targetCollection, metricObject);
									} catch (NullPointerException e) {
										// System.out.println("Null values being
										// ignored...");
//										e.printStackTrace();
									}
								}
							}
						}
						break;
					case "social-following":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									try {
										metricObject.put("widget_array_type", "data_sets");
										metricObject.put("widget_array_position", dsra);
										JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

										try {
											if (dataSetRowObject.get("column_2").toString().equals("")) {
//												System.out.println("Ignore empty strings.");
											} else {
												metricObject.put("metric_key", "social_followers");
												metricObject.put("metric_value",
														Long.parseLong(dataSetRowObject.get("column_2").toString()));
												updateDatabase(targetDatabaseConnectionUri, targetDatabaseName,
														targetCollectionName, targetConnectionString, targetMongoClient,
														targetDatabase, targetCollection, metricObject);
											}
										} catch (NullPointerException e) {
//											System.out.println("Null value found. Ignoring.");
//											e.printStackTrace();
										}
									} catch (Exception e) {
										// System.out.println("ignore nulls");
//										e.printStackTrace();
									}
								}
							}
						}
						break;
					case "staff-utilisation":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						if (cursorObject.containsKey("extras")) {
							dataSetsArray = (JSONArray) cursorObject.get("extras");
							if (dataSetsArray.size() > 0) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
								metricObject.put("widget_array_type", "extras");
								metricObject.put("widget_array_position", 0);
								metricObject.put("metric_key", "percent_staff_utilisation");
								metricObject.put("metric_value", dataSetObject.get("value_2"));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricObject);
							}
						}
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
									metricObject.put("metric_key", "percent_staff_member_utilised");
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("widget_data_entity_type", "employee");
									metricObject.put("widget_data_entity_value",
											dataSetRowObject.get("column_1").hashCode());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "staff-wage-forecast":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", dataSetRowObject.get("column_1").toString()
											.replaceAll("\\.", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "storage-usage":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", dataSetRowObject.get("column_1").toString()
											.replaceAll("\\.", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "timesheets-to-approve":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						// System.out.println(cursorObject.get("object_type").toString());
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_key", dataSetRowObject.get("column_1").toString()
											.replaceAll("\\.", "_").toLowerCase());
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "top-5-expense-categories":
						metricObject.put("metric_date_utc", utcDate.toString());
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						metricObject.put("metric_time_dimension", "day");
						for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
							JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
							metricObject.put("metric_key", "top_expense_categories");
							if (dataSetRowsArray.size() > 0) {
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
									metricObject.put("widget_data_entity_type", "category");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "top-5-oldest-expenses-to-approve":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_key", "oldest_expense_to_approve_number_days");
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("widget_data_entity_type", "expense");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);

									metricObject.put("metric_key", "oldest_expense_to_approve_amount");
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									metricObject.put("widget_data_entity_type", "expense");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "top-performing-campaigns":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						if (cursorObject.containsKey("extras")) {
							dataSetsArray = (JSONArray) cursorObject.get("extras");
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
							metricObject.put("widget_array_type", "extras");
							metricObject.put("widget_array_position", 0);

							metricObject.put("metric_key", "campaign_open_rate");
							metricObject.put("metric_value", dataSetObject.get("value_1"));
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);

							metricObject.put("metric_key", "campaign_click_rate");
							metricObject.put("metric_value", dataSetObject.get("value_2"));
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);
						}
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_key", "campaign_opened_percentage");
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("widget_data_entity_type", "campaign");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);

									metricObject.put("metric_key", "campaign_clicked_percentage");
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									metricObject.put("widget_data_entity_type", "campaign");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "top-selling-categories":
						// System.out.println(cursorObject.get("object_type").toString());
						break;
					case "top-selling-products":
						metricObject.put("metric_date_utc", utcDate.toString());
						dataSetsArray = (JSONArray) cursorObject.get("data_sets");
						metricObject.put("metric_time_dimension", "day");
						for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
							JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
							for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
								metricObject.put("widget_array_type", "data_sets");
								metricObject.put("widget_array_position", dsra);

								JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);
								if (dataSetRowObject.get("column_1").toString().toLowerCase().equals("n/a")
										|| dataSetRowObject.get("column_2").toString().toLowerCase().equals("n/a")) {
									// Ignore "N/A" values
								} else {
									metricObject.put("metric_key", "sales_count_top_selling_product");
									metricObject.put("widget_data_entity_type", "product");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
									metricObject.put("metric_key", "sales_revenue_top_selling_product");
									metricObject.put("widget_data_entity_type", "product");
									metricObject.put("widget_data_entity_value", dataSetRowObject.get("column_1"));
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "top-subscription-usage":
						metricObject.put("metric_date_utc", utcDate.toString());
						metricObject.put("metric_time_dimension", "day");
						if (cursorObject.containsKey("data_sets")) {
							// Iterate through the data_sets data
							dataSetsArray = (JSONArray) cursorObject.get("data_sets");
							for (int dsa = 0; dsa < dataSetsArray.size(); dsa++) {
								JSONObject dataSetObject = (JSONObject) dataSetsArray.get(dsa);
								JSONArray dataSetRowsArray = (JSONArray) dataSetObject.get("rows");
								for (int dsra = 0; dsra < dataSetRowsArray.size(); dsra++) {
									metricObject.put("widget_array_type", "data_sets");
									metricObject.put("widget_array_position", dsra);
									JSONObject dataSetRowObject = (JSONObject) dataSetRowsArray.get(dsra);

									metricObject.put("metric_key", "subscriptions_in_use");
									metricObject.put("metric_value", dataSetRowObject.get("column_2"));
									metricObject.put("metric_subscription_name", dataSetRowObject.get("column_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);

									metricObject.put("metric_key", "subscriptions_purchased");
									metricObject.put("metric_value", dataSetRowObject.get("column_3"));
									metricObject.put("metric_subscription_name", dataSetRowObject.get("column_1"));
									updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
											targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
											metricObject);
								}
							}
						}
						break;
					case "total-leads-won-leads":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("month.this.year")) {
								metricCurrent.put("widget_data_set_name", "month.this.year");
								metricCurrent.put("metric_key", "leads_won");
								metricCurrent.put("metric_time_dimension", "month");
							} else {
								metricPrevious.put("widget_data_set_name", "month.last.year");
								metricPrevious.put("metric_key", "leads_won");
								metricPrevious.put("metric_time_dimension", "month");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("metric_key", "leads_won");
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("metric_key", "leads_won");
							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "website-conversions":
						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("total.conversions")) {
								metricCurrent.put("widget_data_set_name", "total.conversions");
								metricCurrent.put("metric_key", "total_conversions");
								metricCurrent.put("metric_time_dimension", "day");
							} else {
								metricPrevious.put("widget_data_set_name", "conversion.rate");
								metricPrevious.put("metric_key", "conversion_rate");
								metricPrevious.put("metric_time_dimension", "day");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "website-traffic":
						// System.out.println(cursorObject.get("object_type").toString());

						// Convert the 'object_creation_date' to a UTC Date
						// object
						dateObject = (JSONObject) parser.parse(cursorObject.get("object_creation_date").toString());
						utcDate = Instant.ofEpochMilli((Long) dateObject.get("$date")).atZone(ZoneId.of("UTC"))
								.toLocalDateTime();

						// Iterate through the graph data
						graphDataArray = (JSONArray) cursorObject.get("graph_data");
						for (int gda = 0; gda < graphDataArray.size(); gda++) {
							JSONObject graphArrayObject = (JSONObject) graphDataArray.get(gda);
							String dataSetName = (String) graphArrayObject.get("data_set_name");

							if (dataSetName.equals("unique.visits")) {
								metricCurrent.put("widget_data_set_name", "unique.visits");
								metricCurrent.put("metric_key", "unique_visits");
								metricCurrent.put("metric_time_dimension", "day");
							} else {
								metricPrevious.put("widget_data_set_name", "repeat.visits");
								metricPrevious.put("metric_key", "repeat_visits");
								metricPrevious.put("metric_time_dimension", "day");
							}
						}

						// Iterate through the metric data
						extrasArray = (JSONArray) cursorObject.get("extras");
						for (int ea = 0; ea < extrasArray.size(); ea++) {
							JSONObject extrasObject = (JSONObject) extrasArray.get(ea);
							metricCurrent.put("widget_array_type", "extras");
							metricCurrent.put("widget_array_position", ea);
							metricCurrent.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricCurrent.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),extrasArray.size(), utcDate).toString());
								metricCurrent.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricCurrent);
							} catch (NullPointerException e) {
//								metricCurrent.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}

							metricPrevious.put("widget_array_type", "extras");
							metricPrevious.put("widget_array_position", ea);
							metricPrevious.put("widget_label_key", extrasObject.get("label_key").toString());
							try {
								metricPrevious.put("metric_date_utc", calculateDate(extrasObject.get("label_key").toString(),(extrasArray.size()*2), utcDate).toString());
								metricPrevious.put("metric_value",
										Double.parseDouble(extrasObject.get("value_1").toString()));
								updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
										targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
										metricPrevious);
							} catch (NullPointerException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore null values
							} catch (NumberFormatException e) {
//								metricPrevious.put("metric_value", 0);
								// Ignore invalid values
							}
						}
						break;
					case "working-today":
						metricObject.put("metric_date_utc", utcDate.toString());
						if (cursorObject.containsKey("extras")) {
							dataSetsArray = (JSONArray) cursorObject.get("extras");
							JSONObject dataSetObject = (JSONObject) dataSetsArray.get(0);
							metricObject.put("widget_array_type", "extras");
							metricObject.put("widget_array_position", 0);

							metricObject.put("metric_key", "total_working_today");
							metricObject.put("metric_value", dataSetObject.get("value_1"));
							metricObject.put("metric_time_dimension", "day");
							updateDatabase(targetDatabaseConnectionUri, targetDatabaseName, targetCollectionName,
									targetConnectionString, targetMongoClient, targetDatabase, targetCollection,
									metricObject);
						}
						break;

					default:
						// values_not_caught_above;

					}

				} catch (ParseException e) {
					System.out.println("JSON PARSE EXCEPTION!!!");
					System.out.println(cursor.next());
					e.printStackTrace();
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					System.out.println("Check the data types on this document");
				} catch (NullPointerException e) {
					e.printStackTrace();
					System.out.println("Missing required data elements");
				}

			}
		} finally {
			cursor.close();
		}

		// Close the connection
		sourceMongoClient.close();
		targetMongoClient.close();
	}

	public static void updateDatabase(String databaseConnectionUri, String targetDatabaseName,
			String targetCollectionName, MongoClientURI targetConnectionString, MongoClient targetMongoClient,
			MongoDatabase targetDatabase, MongoCollection<Document> targetCollection, JSONObject metricObject) {

		Document docMetric = Document.parse(metricObject.toString());
		targetCollection.updateOne(
				and(eq("widget_party_uuid", metricObject.get("widget_party_uuid")),
						eq("widget_data_entity_value", metricObject.get("widget_data_entity_value")),
						eq("widget_data_entity_type", metricObject.get("widget_data_entity_type")),
						eq("metric_key", metricObject.get("metric_key")),
						eq("widget_data_set_name", metricObject.get("widget_data_set_name")),
						eq("widget_label_key", metricObject.get("widget_label_key")),
						eq("widget_object_creation_date", metricObject.get("widget_object_creation_date")),
						eq("widget_object_type", metricObject.get("widget_object_type")),
						eq("widget_oid", metricObject.get("widget_oid")),
						eq("widget_array_position", metricObject.get("widget_array_position"))),
				new Document("$set", docMetric), new UpdateOptions().upsert(true));
	}

	public static LocalDateTime calculateDate(String labelKey, int arraySize, LocalDateTime utcDate){

		String[] tokens = labelKey.split("\\.");
		long increment = Long.parseLong(tokens[1]);

		switch (tokens[0].toLowerCase()) {
		case "m":
			utcDate = utcDate.minusMonths(arraySize - increment);
			break;
		case "w":
			utcDate = utcDate.minusWeeks(arraySize - increment);
			break;
		case "d":
			utcDate = utcDate.minusDays(arraySize - increment);
			break;
		default:
			// values_not_caught_above;
		}
		
		return utcDate;
	}

}
